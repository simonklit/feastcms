<html>
<head>
<title>FeastCMS | Editing</title>

<!-- Import of stylesheets, javascripts and PHP includes -->
<?php 
    include 'includes/imports.php';
?>

</head>
<body>
<?php
//Start session so that we can access session data
session_start();

?>
<script type="text/javascript">
window.onbeforeunload = confirmExit;
function confirmExit()
{
 return "Please note that any entered information will be lost.";
}
 
//For unbinding
$('.editform').submit(function(event){
 window.onbeforeunload = null;
});
</script>
<?php

//Include database connection
include 'includes/dbcon.php';

//Check if user is logged in
if ($_SESSION['loggedin'] == 0) { //If user is not logged in
    header('Location: index.php'); //Redirect to index page
}else{ //User is logged in

//Set variables
$page = $_GET['p'];

//Get content data from database
$query = mysql_query("SELECT * FROM content WHERE page = '$page' ORDER BY cid ASC"); //Set sorting to be ascending ordered by the cid
?>
<div class="content animated bounceInDown">
<?php echo displayHeader();?>
<div class="editcontent">
<h3>You are editing: <?php echo $page;?></h3>
<?php
//If content has been deleted already, display message
if ($_SESSION['deleted'] == 1) {
    echo "The content has been succesfully deleted!";
    $_SESSION['deleted'] = 0;
}
//Display content from database
$i = 0;
$numrows = mysql_num_rows($query);
while($row = mysql_fetch_array($query))
  {
$i++
?> 
<div class="indcontent">
<div class="indform">
<span class="query<?php echo $i;?>"><span class="formheader <?php echo $i; ?>"><?php echo $row['cid'];?></span>
<form action="auths/submitedit.php" method="post">
<input type="text" name="cid" id="cid" class="hidden t<?php echo $i; ?> text-field left" value="<?php echo $row['cid'];?>"></span>
<textarea cols="85" rows="8" name="texta" id="texta"><?php echo $row['content'];?></textarea>
<input type="checkbox" id="<?php echo $i; ?>" value="1" name="strip" <?php if($row['strip'] == 0) { echo ''; }else{echo 'checked="yes"';}?>> <label for="<?php echo $i; ?>" class="noselect">Disable HTML?</label>
<input type="hidden" value="<?php echo $row['page'];?>" id="pagename" name="pagename">
<input type="hidden" value="<?php echo $row['cid'];?>" id="coid" name="coid">
<a class="button blue bot" onclick="$(this).closest('form').submit()">Save</a>
<a class="button red bot2" href="<?php echo "delete.php?cid=" . $row['cid'] . "&page=" . $row['page']?>">Delete</a>
</form>
<?php if ($numrows > $i) {echo "<hr>";}?></div></div>
<?php

//Close MySQL connection
mysql_close($con);?>
<script>
$( '.query<?php echo $i;?>' ).click(function() {
  $( ".<?php echo $i; ?>" ).hide();
  $(".t<?php echo $i;?>").show();
});
</script><?php
  }
?>
</div><?php echo displayFooter();?></div>
<?php
}
?>