<html>
<head>
<title>FeastCMS | Add Content</title>

<?php
//Imports
    include 'includes/imports.php';
?>
</head>
<body>
<?php
session_start(); //Start session, so that we can access session data

if ($_SESSION['loggedin'] == 0) { 
//User is not logged in
header('Location: index.php'); //Redirect to index
}else{
//User is logged in
?>
<div class="content animated bounceInDown">
<?php echo displayHeader();?>
<div class="editcontent">
<h3>Adding new content!</h3>
<?php
if ($_SESSION['isadded'] == 1) {
    echo "Content has been succesfully added!";
    $_SESSION['isadded'] = 0;
}elseif($_SESSION['isadded'] == 2) {
    echo "Feast could not connect to the database! Consult your webmaster.";
        $_SESSION['isadded'] = 0;
}
?>
<div class="indcontent">
<div class="indform">
<form action="auths/submitadd.php" method="post">
<input type="text" name="cid" id="cid" class="text-field" placeholder="Enter content id">
<input type="text" name="page" id="page" class="text-field" placeholder="Enter page">
<textarea cols="85" rows="8" name="texta" id="texta"></textarea>
<input type="checkbox" id="1" value="1" name="strip" > <label for="1" class="noselect">Disable HTML?</label>
<input type="hidden" value="<?php echo $row['page'];?>" id="pagename" name="pagename">
<input type="hidden" value="<?php echo $row['cid'];?>" id="coid" name="coid">
<a class="button blue bot" onclick="$(this).closest('form').submit()">Add</a>
</form>
</div>
</div>
</div>
<a class="button blue" href="register.php">Add administrator</a>
<a class="button red" href="auths/logout.php">Log out</a>
<?php echo displayFooter();?>
</div>
<?php
}
?>
</body>
</html>