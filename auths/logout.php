<?php
session_start();    // Create the session so that data can be saved to session
session_destroy(); //Destroy the session and all of it's data
header('Location: /feast/'); //Redirect to index page

?>
