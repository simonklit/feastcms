<script type="text/javascript">

$(document).ready(function () {
    
    $("#slideDown").slideDown("slow");

    $(".button").mouseenter(function () {
        $(this).fadeTo(140, 0.8);
    });
    $(".button").mouseleave(function () {
        $(this).fadeTo(140, 1);
    });
window.onbeforeunload = function() {
};

});

$(document).ready(function(){
    $('textarea').autosize({append: "\n"});   
});

//Script to make div's with the clickable class clickable
$(document).delegate(".clickable", "click", function() {
   window.location = $(this).find("a").attr("href");
});
</script>