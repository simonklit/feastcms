#FeastCMS
Build 0003

##Current features
* Login with MD5 hash
* Add administrator with MD5 hash
* Fetch and display content from database on admin index page, ordered by page
* Edit content, ordered by page
* Add content, with possibility to disable HTML tags
* Function to fetch content from database and display on actual parent website
* Delete content per cid (b0002)
* Edit cid by clicking on cid header on edit page (b0003)

##Upcoming features
* Delete content per page
* Navigation bar
* Configuration process to set up first admin user and configure the database, when Feast is being setup for a new website
* Additional login security with MD5 salt
* WYSIWYG editor for non-developers using Feast
* DRY code optimization
* General code optimization (Changing db handling to MySQLi or PDO, functioning universal stylesheet linking, include linking etc.)
* And potentially much more, which we will discover as time goes by..