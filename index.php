<html>
<head>
<title>FeastCMS | Overview</title>

<!-- Import of stylesheets, javascripts and PHP includes -->
<?php 
    include 'includes/imports.php';
?>

</head>
<body>

<?php
session_start(); //Start session so that we can access session data


if ($_SESSION['loggedin'] == 0) { 
//User is not logged in
//Change animation to shake if input is incorrect
if($_SESSION["incorrect"] == 1){echo '<div class="formcontent animated shake fast">';}else{echo '<div class="formcontent animated bounceInDown">';}
?>
<?php echo displayHeader();?>
<h3>Admin Login</h3>
<?php if($_SESSION["incorrect"] == 1){ echo '<div class="hidden" id="slideDown">Incorrect username and/or password!</div>'; $_SESSION['incorrect'] = 0;} ?>
<form id="login" name="login" method="post" action="auths/login.php">

 <input type="text" name="username" id="username" class="text-field" placeholder="Username" autocomplete="off" autofocus />
 <input type="password" name="password" id="password" class="text-field" placeholder="Password"/>
 <input type="submit" name="button" id="button" id="login" value="Log in" class="button blue"/>
 </form><?php echo displayFooter();?></div>
 <?php
}
//User is logged in
else {
    ?>
<div class="content animated bounceInDown">
<?php echo displayHeader();?>
<?php
include 'content.php';
?>
<a class="button blue" href="register.php">Add administrator</a>
<a class="button blue" href="add.php">Add content</a>
<a class="button red" href="auths/logout.php">Log out</a>
<?php echo displayFooter();?>
</div>
<?php
}
?>

</body>
</html>