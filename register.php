<html>
<head>
<title>FeastCMS | Register Admin</title>

<!-- Import of stylesheets, javascripts and PHP includes -->
<?php 
    include 'includes/imports.php';
    include 'scripts/script.js';
?>

</head>
<body>
<?php
session_start(); //Start session so that we can access session data

//Check if user is logged in
if ($_SESSION['loggedin'] == 0) {
//User is not logged in
header('Location: /feast/'); //Redirect to index page
}
//User is logged in
else {
?>
<div class="formcontent animated bounceInDown">
<div class="headertext"><a href="index.php">FeastCMS</a></div>
<h3>Add an administrator!</h3>
<form id="register" name="register" method="post" action="auths/admreg.php">
<?php

/* Begin checks */
if($_SESSION["registered"] == 1){echo "Admin has been added!"; $_SESSION['registered'] = 0;}
if($_SESSION["norep"] == 1){echo "The two passwords did not match! Try again!"; $_SESSION['norep'] = 0;}
if($_SESSION["noat"] == 1){echo "You have not entered a valid e-mail address. Try again!"; $_SESSION['noat'] = 0;}
if($_SESSION["fempty"] == 1){echo "Some fields are empty. Try again!"; $_SESSION['fempty'] = 0;}
/* End checks */

?>
 <input type="text" class="text-field" name="username" id="username" placeholder="Username" autocomplete="off" autofocus />
 <input type="text" class="text-field" name="email" id="email" placeholder="E-mail address"/>
 <input type="password" class="text-field" name="password" id="password" placeholder="Password"/>
  <input type="password" class="text-field" name="reppassword" id="reppassword" placeholder="Repeat Password"/>
 <input type="submit" id="button" name="button" value="Add" class="button blue" />
</form>
<?php echo displayFooter();?>
</div>
<?php
}
?>
</body>
</html>