<html>
<head>
<title>FeastCMS | Delete</title>
<?php
//Import imports!
    include 'includes/imports.php';
    include 'includes/dbcon.php'; //Database connection included
?>
</head>
<body>
<?php
session_start();

if ($_SESSION['loggedin'] == 0) { 
//User is not logged in
header('Location: index.php'); //Redirect to index
}else{
if ($_GET['s'] != "y"){
$_SESSION['page'] = $_GET['page'];
$_SESSION['cid'] = $_GET['cid'];
}
if ($_GET['s'] == "y") {
//Set variables
$page = $_SESSION['page'];
$cid = $_SESSION['cid'];

//Generate query
$query = mysql_query("DELETE FROM content WHERE page = '$page' AND cid = '$cid'");
header('Location: edit.php?p='.$page); //Redirect to index
$_SESSION['deleted'] = 1;
}else{
?>
<div class="content animated bounceInDown">
<?php echo displayHeader();?>
<div class="editcontent">
<h3>Are you sure you want to delete <?php echo $_GET['cid'] . " on " . $_GET['page'];?>?</h3>
<a class="button blue" href="delete.php?s=y">Yes, delete it</a>
<a class="button red" href="<?php echo "edit.php?p=" . $_GET['page'];?>">No, take me back</a>
</div></div>
<?php
}
}
?>
</body>
</html>